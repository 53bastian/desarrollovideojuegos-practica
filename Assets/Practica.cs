using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Practica : MonoBehaviour
{
    public List<string> ranks;
    public float timerValue = 90;
    public float timerMin = 0;
    public Text txtOrbs, txtPoints, txtDamage, txtItems, txtRangoTotal, txtRangoTimer, txtRangoOrbs, txtRangoPoint, txtRangoDamage, txtRangoItems;
    public Text timerText;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timerValue > 0)
        {
            timerValue += Time.deltaTime;
            //Time.timeScale = 1;

        }
        else
        {
            timerValue = 0;
        }
        DisplayTime(timerValue);
    }
    void DisplayTime(float timeToDisplay)
    {
        if (timeToDisplay<0)
        {
            timeToDisplay = 0;
        }

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        float milliseconds =  timeToDisplay % 1 * 1000;

        //float m = Random.Range(0, 10);
        //minutes = m;
        //float s = Random.Range(0, 60);
        //seconds = s;
        //float mi = Random.Range(0, 500);
        //milliseconds = mi;
        timerText.text = string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds,milliseconds);
    }

    public void Ranks(float timer, float orbs, float points, float damage, float items)
    {
        ranks = new List<string>();
        ranks.Add("C");
        ranks.Add("B");
        ranks.Add("A");
        ranks.Add("S");

        orbs = 4000;//Random.Range(1000, 9999);
        txtOrbs.text = "" + orbs;
        points = 4000;///Random.Range(1000, 9999);
        txtPoints.text = "" + points;
        damage = 2;//Random.Range(0, 6);
        txtDamage.text = "" + damage;
        items = 2; //Random.Range(0, 6);
        txtItems.text = "" + items;

        //rango de orbes
        if (orbs >= 7000){
            txtRangoOrbs.text = "S";
        }else if (orbs>=6000 && orbs <7000) {
            txtRangoOrbs.text = "A";
        }else if(orbs>= 5000 && orbs<6000){
            txtRangoOrbs.text = "B";
        }else if (orbs <=4999) {
            txtRangoOrbs.text = "C";
        }

        //rango de puntos
        if (points >= 7000){
            txtRangoPoint.text = "S";
        }
        else if (points >= 6000 && points < 7000){
            txtRangoPoint.text = "A";
        }
        else if (points >= 5000 && points < 6000){
            txtRangoPoint.text = "B";
        }
        else if (points <= 4999){
            txtRangoPoint.text = "C";
        }

        //rango total 
        if (orbs >= 5000 && points >= 5000 || items ==5 || damage ==5){
            txtRangoTotal.text = "S";
        }else if (orbs>=5000 && points <=4999 || items == 4 || damage == 4){
            txtRangoTotal.text = "A";
        }else if (points>=5000 && orbs <=4999 || items==3 || damage==3){
            txtRangoTotal.text = "A";
        }else if (orbs <= 4999 && points <= 4999 || items<=2 || damage<=2){
            txtRangoTotal.text = "B";
        }

        //rango damage
        if (damage ==0 ){
            txtRangoDamage.text = "C";
        } else if (damage==1){
            txtRangoDamage.text = "B";
        } else if (damage== 2 || damage ==3){
            txtRangoDamage.text = "A";
        }else if (damage==4 || damage ==5){
            txtRangoDamage.text = "S";
        }

        //rango items
        if (items == 0){
            txtRangoItems.text = "C";
        }else if (items == 1){
            txtRangoItems.text = "B";
        }else if (items == 2 || items == 3){
            txtRangoItems.text = "A";
        }else if (items == 4 || items ==5){
            txtRangoItems.text = "S";
        }



    }
    public void xd()
    {
        Ranks(0,0,0,0,0);
    }
}
